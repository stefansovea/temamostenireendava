﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemaMostenireEndava.Persoane;
using TemaMostenireEndava.Vehicule;

namespace TemaMostenireEndava
{
    class Program
    {
        static void Main(string[] args)
        {
            Controlor c1 = new Controlor("Radu",20, 2000);
            c1.bonusSalarial = 200;           
            Console.WriteLine(c1);
            Pasager p1 = new Pasager("Ion", 24, "student", 100);
            Console.WriteLine(p1);
            Sofer s1 = new Sofer("Vasile", 30, 2000);
            Console.WriteLine(s1);
            Autobuz a1 = new Autobuz("B121ATB", false, 3);
            Console.WriteLine(a1);
            Metrou m1 = new Metrou("M1B", "Astra", 4);
            Console.WriteLine(m1);
            Troleibuz t1 = new Troleibuz("B321TRB", 4);
            Console.WriteLine(t1);

            Console.ReadKey();
        }
    }
}
