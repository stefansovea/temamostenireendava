﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaMostenireEndava
{
    public class Vehicul
    {
        public string nrInmatriculare { get; set; }
        public int Capacitate { get; set; }
        public bool Electric { get; set; }
        override public string ToString()
        {
            return "Numar de inmatriculare: " + nrInmatriculare + " capacitate: " + Capacitate + " Electric: " + Electric;
        }
    }
}
