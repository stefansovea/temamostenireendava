﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaMostenireEndava.Vehicule
{
    public class Troleibuz:Vehicul
    {
        public int nrAparate { get; set; }
        public Troleibuz(string _nrInmatriculare)
        {
            nrInmatriculare = _nrInmatriculare;
            Electric = true;
            Capacitate = 95;
        }
        public Troleibuz(string _nrInmatriculare, int _nrAparate)
        {
            nrInmatriculare = _nrInmatriculare;
            Electric = true;
            Capacitate = 95;
            nrAparate = _nrAparate;
        }

        public override string ToString()
        {
            return base.ToString() + " aparate: "+nrAparate;
        }
    }
}
