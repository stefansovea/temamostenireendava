﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaMostenireEndava.Vehicule
{
    public class Metrou:Vehicul
    {
        public string tipLocomotiva { get; set; }
        public int nrVagoane { get; set; }
        public Metrou(string _nrInmatriculare)
        {
            nrInmatriculare = _nrInmatriculare;
            Electric = true;
            Capacitate = 205;
        }
        public Metrou (string _nrInmatriculare, string _tipLocomotiva, int _nrVagoane)
        {
            nrInmatriculare = _nrInmatriculare;
            Electric = true;
            Capacitate = 205;
            tipLocomotiva = _tipLocomotiva;
            nrVagoane = _nrVagoane;
        }

        public override string ToString()
        {
            return base.ToString() + " Tip locomotiva: " + tipLocomotiva + " nr vagoane: " + nrVagoane;
        }
    }
}
