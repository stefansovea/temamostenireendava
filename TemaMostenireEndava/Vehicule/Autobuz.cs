﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaMostenireEndava
{
    public class Autobuz:Vehicul
    {
        public bool aerConditionat { get; set; }
        public int nrAutomateVerificare { get; set; }
        public Autobuz (string _nrInmatriculare)
        {
            nrInmatriculare = _nrInmatriculare;
            Electric = false;
            Capacitate = 75;
        }
        public Autobuz(string _nrInmatriculare, bool _aerConditionat, int _nrAutomate)
        {
            nrInmatriculare = _nrInmatriculare;
            Electric = false;
            Capacitate = 75;
            aerConditionat = _aerConditionat;
            nrAutomateVerificare = _nrAutomate;
        }
        public override string ToString()
        {
            return base.ToString()+" AC: "+aerConditionat+" nr automate: "+nrAutomateVerificare;
        }
    }
}
