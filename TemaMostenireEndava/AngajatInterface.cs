﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaMostenireEndava
{
    interface AngajatInterface
    {
        int GetSalariu();
        int GetBonusLunar();
        void SetBonusLunar(int bonus);
    }
}
