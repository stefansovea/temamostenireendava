﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaMostenireEndava
{
    public abstract class Statie
    {
        public abstract string GetName();
        public abstract string GetLocation();
    }
}
