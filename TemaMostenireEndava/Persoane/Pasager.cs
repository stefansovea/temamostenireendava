﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaMostenireEndava.Persoane
{
    class Pasager:Persoana
    {
        public string categorieSpeciala { get; set; }
        public int creditCalatorie { get; set; }
        public Pasager(string _nume, int _varsta)
        {
            nume = _nume;
            varsta = varsta;
        }
        public Pasager(string _nume, int _varsta, string _categorieSpeciala, int _creditCalatorie)
        {
            nume = _nume;
            varsta = _varsta;
            categorieSpeciala = _categorieSpeciala;
            creditCalatorie = _creditCalatorie;
        }

        public override string ToString()
        {
            return base.ToString()+ " categorie speciala: "+categorieSpeciala+ ", credit: "+creditCalatorie;
        }
    }
}
