﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaMostenireEndava.Persoane
{
    public class Controlor:Persoana,AngajatInterface
    {
        public int salariu { get; set; }
        public int bonusSalarial { get; set; }
        public Controlor(string _nume, int _varsta)
        {
            nume = _nume;
            varsta = _varsta;
        }
        public Controlor(string _nume, int _varsta, int _salariu)
        {
            nume = _nume;
            varsta = _varsta;
            salariu = _salariu;
        }

        public int GetSalariu()
        {
            return salariu;
        }

        public int GetBonusLunar()
        {
            return bonusSalarial;
        }

        public void SetBonusLunar(int bonus)
        {
            bonusSalarial = bonus;
        }

        public override string ToString()
        {
            return base.ToString()+" salariu: "+salariu+" bonus salarial: "+bonusSalarial;
        }
    }
}
