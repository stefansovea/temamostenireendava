﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaMostenireEndava.Persoane
{
    class Sofer:Persoana
    {
        public int salariu { get; set; }
        public int bonusSalarial { get; set; }
        public bool amenzi { get; set; }
        public Sofer(string _nume, int _varsta)
        {
            nume = _nume;
            varsta = _varsta;
        }
        public Sofer(string _nume, int _varsta, int _salariu)
        {
            nume = _nume;
            varsta = _varsta;
            salariu = _salariu;
            amenzi = false;
        }

        public int GetSalariu()
        {
            return salariu;
        }

        public void SetAmenda()
        {
            amenzi = true;
            salariu -= 200;
        }

        public int GetBonusLunar()
        {
            return bonusSalarial;
        }

        public void SetBonusLunar(int bonus)
        {
            bonusSalarial = bonus;
        }

        public override string ToString()
        {
            return base.ToString() + " salariu: " + salariu + " bonus salarial: " + bonusSalarial+" amenda: "+amenzi;
        }
    }
}
