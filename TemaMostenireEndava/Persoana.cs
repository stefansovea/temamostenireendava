﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemaMostenireEndava
{
    
    public class Persoana:System.Attribute
    {       
        public string nume { get; set; }
        public int varsta { get; set; }

      

        override public string ToString ()
        {
            return "Nume: " + nume + ", varsta: " + varsta;
        }

    }
}
